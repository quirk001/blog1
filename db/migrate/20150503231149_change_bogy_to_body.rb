class ChangeBogyToBody < ActiveRecord::Migration
  def change
	rename_column :posts, :bogy, :body
	rename_column :comments, :bogy, :body
  end
end
